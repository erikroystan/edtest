$(document).ready(function(){

function displayMessages(data)
{
	$.each(data.results, function(i, result){	
		content = "<li>";
		content += "<p>" + result.text + "</p>";
		content += "<time>" + result.created_at + "</time>";
		
		content += "<div class=\"message-details\">";
		content += "<span>Message #" + result.id + "</span>";
		content += "<a href=\"" + result.url + "\">Permalink</a>";
		content += "</div>";
		
		content += "<button class=\"details\" id=\"" + result.id + "\">Details</button>";
		content += "<button class=\"delete\" id=\"" + result.id + "\">Delete</button>";	
		content += "</li>";
		$(content).appendTo(".messages");
	});
	
	if (data.next != null)
	{
		$.getJSON(data.next, function(data){
			displayMessages(data);	
		});
	} 
	else 
	{			
		$(".message-details").hide();

		$("button.details").click(function(){
			$(this).siblings(".message-details").toggle();
		});

		$("button.delete").click(function(){
			$.ajax({
				url: "https://erik-tech-test.herokuapp.com/messages/" + event.target.id,
				type: "DELETE",
				success: function(result) {
					location.reload(true);
				}
			});
		});
	}
}

// Request the first (or possibly only) page of messages
$.getJSON("https://erik-tech-test.herokuapp.com/messages/", function(data){
	displayMessages(data);	
});

$("button.submit").click(function(){
	$.ajax({
		url: "https://erik-tech-test.herokuapp.com/messages/" + event.target.id,
		type: "POST",
		data: "text=" + $(this).siblings("textarea").val(),
		success: function(result) {
			location.reload(true);
		}
	});
});

});